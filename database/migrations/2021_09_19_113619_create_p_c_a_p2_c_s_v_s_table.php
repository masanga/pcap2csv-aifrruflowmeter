<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePCAP2CSVSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_c_a_p2_c_s_v_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            
            $table->string('pcapName')->nullable();
            $table->string('pcapSize')->nullable();
            $table->string('csvSize')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_c_a_p2_c_s_v_s');
    }
}
