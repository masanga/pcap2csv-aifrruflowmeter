<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PCAP2CSV extends Model
{
    //Fillable
    protected $fillable = [

    	'userId', 'pcapName', 'pcapSize', 'csvSize'

    ];
}
