<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //Fillables
    protected $fillable = [

    	'userId', 'subscriptionType', 'daysLeft'

    ];
}
