<?php

namespace App\Http\Controllers;

use App\User;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    //updateSettings
    public function updateSettings(Request $request) {
    	//Updating Setting
    	$settingsData = Setting::where('userId', Auth::User()->id)->get()->toArray();

    	if (sizeof($settingsData) == 1) {

    		//Updating Settings
    		$updateSettings = Setting::find($settingsData['0']['id']);
    		$updateSettings->webHookUrl = $request->webHookUrl;

    		if ($updateSettings->update()) {
    			$message = "Settings Was Updated Successfully";
    			return redirect()->back()->with(['successMessage' => $message]);
    		}else {
    			$message = "Failed to Update Settings";
    			return redirect()->back()->with(['errorMessage' => $message]);
    		}

    	}else {
    		$message = "Failed to Update Settings";
    		return redirect()->back()->with(['errorMessage' => $message]);
    	}
    }
}
