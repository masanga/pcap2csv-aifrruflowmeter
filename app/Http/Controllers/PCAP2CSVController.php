<?php

namespace App\Http\Controllers;

use Storage;
use App\PCAP2CSV;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PCAP2CSVController extends Controller
{

	//postConvertPcapToCSV
	public function postConvertPcapToCSV(Request $request) {

		ini_set("max_execution_time", 3600);

		//Form Validation
		$this->validate($request,
				[
					'pcapName' => 'required',
					'pcapFile' => 'required'
				]);

		if (!Auth::check()) {
			$message = "You need to login to convert pcap to csv";
			return redirect()->back()->with(['errorMessage' => $message]);
		}

		//Adding New Task
		$newPcapTask = new PCAP2CSV();
		$newPcapTask->userId = Auth::User()->id;
		$newPcapTask->pcapName = $request->pcapName;
		$newPcapTask->pcapSize = "-";
		$newPcapTask->csvSize = "-";

		if ($newPcapTask->save()) {

			//Uploading Pcap File
			Storage::disk('public')->putFileAs('pcaps/'.$newPcapTask->id, $request->file('pcapFile'), "pcap.pcap");

			//Converting Pcap to CSV
			$pcap_path = str_replace("\\", "/", Storage::disk('public')->path('pcaps/'.$newPcapTask->id."/"));

			$cmd_path = 'java -jar "'. javaAppsLocation() . 'AifrruFlowMeterV1-0.0.1-SNAPSHOT.jar" ' .' "'.$pcap_path .'"'. ' ' .'"'.$pcap_path.'"';

			shell_exec($cmd_path);

			//Redirect to Download Page
			return redirect()->back()->with([
						'pcapSucess' => 'TRUE', 
						'file_id' => $newPcapTask->id
						]);

		}else {
			$message = "Failed to Convert PCAP to CSV. Please Try Again Later";
			return redirect()->back()->with(['errorMessage' => $message]);
		}


	}

	/**
	 *
	 * @OA\Post(
	 *     path="/api/v1/convert/pcap/to/csv",
	 *     tags={"PCAP2CSV"},
	 *     summary="Create New PCAP to CSV Task",
	 *     description="Create New PCAP to CSV Task",
	 *     @OA\Response(
	 *         response="default",
	 *         description="Successful operation"
	 *     ),
	 *
	 * )
	 *
	 */

	//Convert Pcap to CSV API
	public function apiConvertPcapToCsv(Request $request)
	{

	}

    //convertPcapToCsv
    public function convertPcapToCsv()
    {
    	return view("pages.convertPcapToCsv");
    }
}
