<?php

namespace App\Http\Controllers;

use App\User;
use App\Setting;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //settings
    public function settings()
    {
        //get User Settings
        $userSettings = Setting::where('userId', Auth::User()->id)->get()->toArray();

        return view("pages.settings",
                    [
                        'webHookUrl' => $userSettings['0']['webHookUrl']
                    ]);
    }

    //csvFeatures
    public function csvFeatures()
    {
        return view("pages.csvFeatures");
    }

    //Pricing
    public function pricing()
    {
    	return view("pages.pricing");
    }

    //Index
    public function index()
    {
    	return view("index");
    }
}
