<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{
    //Fillable
    protected $fillable = [

    	'userId', 'os', 'ip', 'browser'

    ];
}
