<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Protected Routes


//updateSettings
Route::post('/updateSettings', ['uses' => 'SettingsController@updateSettings'])->name('updateSettings');

//settings
Route::get('/settings', ['uses' => 'Controller@settings'])->name('settings');



//Convert Pcap to CSV
Route::post('/postConvertPcapToCSV', ['uses' => 'PCAP2CSVController@postConvertPcapToCSV'])->name('postConvertPcapToCSV');

//CSV Features
Route::get('/csv/features', ['uses' => 'Controller@csvFeatures'])->name('csv-features');

//Convert PCAP to CSV
Route::get('/convert/pcap/to/csv', ['uses' => 'PCAP2CSVController@convertPcapToCsv'])->name('convert-pcap-to-csv');

//Pricing Page
Route::get('/pricing', ['uses' => 'Controller@pricing',
						  'as' => 'pricing']);

//Index Page
Route::get('/', ['uses' => 'Controller@index'])->name('index');

//Auth Routes
Auth::routes();

//Home Route
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
