<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $appName }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset("$pF/css/custom.css") }}" rel="stylesheet">

    @include ("incs.head")

</head>

<body>
    <div id="app">
        <nav class="navbar navbar-custom" style="margin: 0%;">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bg-white"></span>
                <span class="icon-bar bg-white"></span>
                <span class="icon-bar bg-white"></span>
              </button>
              <a class="navbar-brand color-white" href="{{ route('index') }}">{{ $appName }}</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                
                @if (Auth::check())
                    <li class="active"><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard <span class="sr-only"></span></a></li>
                @else
                    <li class="active"><a href="{{ route('index') }}"><i class="fa fa-home" aria-hidden="true"></i> Home <span class="sr-only"></span></a></li>
                @endif

                <li><a href="{{ route('convert-pcap-to-csv') }}"><i class="fa fa-hourglass-start" aria-hidden="true"></i> Convert PCAP to CSV</a></li>

                @if (Auth::check())
                    <li><a href="{{ route('settings') }}"><i class="fa fa-cogs" aria-hidden="true"></i> Settings</a></li>                
                @else
                    <li><a href="{{ $pF }}/api/documentation" target="_blank"><i class="fa fa-braille" aria-hidden="true"></i> REST API</a></li>

                    <li><a href="{{ route('csv-features') }}"><i class="fa fa-list-ul" aria-hidden="true"></i> CSV Features</a></li>
{{-- 
                    <li><a href="{{ route('pricing') }}"><i class="fa fa-usd" aria-hidden="true"></i> Pricing</a></li> --}}
                @endif

              </ul>
         
              <ul class="nav navbar-nav navbar-right">

                @if (Auth::check()) 
                    <li>
                        <a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Welcome {{ Auth::User()->firstName }}!</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            <i class="fa fa-share" aria-hidden="true"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @else
                    <li><a href="{{ route('login') }}"><i class="fa fa-user-o" aria-hidden="true"></i> Login</a></li>

                    <li><a href="{{ route('register') }}"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> Register</a></li>
                @endif

              </ul>
            </div>

          </div>
        </nav>

        <main class="py-4">

            @yield('content')

            @include("incs.sessionMessages")
        
        </main>
    </div>

    @include ("incs.footer")
    
</body>
</html>
