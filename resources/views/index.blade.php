@extends('layouts.app')

@section("pageTitle") {{ "Home" }} @endsection

@section('content')


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="{{ asset("$pF/pics/slider_01.jpg") }}" alt="..." style="width: 100%; height: 600px;">
      <div class="carousel-caption">
        <h1>PCAP to CSV</h1>
        <p>Convert PCAP to CSV and get 80+ features in your csv</p>
      </div>
    </div>
 
  </div>

</div>

<br/>

<div class="row">
  <div class="col-sm-6 col-md-3">
    <div class="thumbnail">
      <img src="{{ asset("$pF/pics/features.png") }}" style="width: 200px;" alt="...">
      <div class="caption">
        <h3 class="center">80+ PCAP Features</h3>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-3">
    <div class="thumbnail">
      <img src="{{ asset("$pF/pics/convert.png") }}" alt="..." style="width: 225px;">
      <div class="caption">
        <h3 class="center">Convert PCAP to CSV</h3>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-3">
    <div class="thumbnail">
      <img src="{{ asset("$pF/pics/restful_api.png") }}" alt="..." style="width: 225px;">
      <div class="caption">
        <h3 class="center">REST API</h3>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-3">
    <div class="thumbnail">
      <img src="{{ asset("$pF/pics/documentation.png") }}" alt="..." style="width: 220px;">
      <div class="caption">
        <h3 class="center">API Documentation</h3>
      </div>
    </div>
  </div>
</div>



@endsection