<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" type="image/png" href="{{ env('CMS_FAVICON') }}" sizes="16x16">

{{-- Linking Jquery --}}

<script type="text/javascript" src="{{ asset("$pF/js/jquery.min.js") }}"></script>

{{-- Linking Bootstrap JS --}}
<script type="text/javascript" src="{{ asset("$pF/styles/bts/js/bootstrap.min.js") }}"></script>

{{-- Linking Bootstrap CSS --}}
<link rel="stylesheet" type="text/css" href="{{ asset("$pF/styles/bts/css/bootstrap.min.css") }}">

{{-- Linking APP JS --}}
<script type="text/javascript" src="{{ asset("$pF/js/app.js") }}"></script>


{{-- Linking Font Awesome 4.7.0 --}}
<link rel="stylesheet" type="text/css" href="{{ asset("$pF/styles/font-awesome-4.7.0/css/font-awesome.min.css") }}">

{{-- SweetAlert2 --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

{{-- Custom CSS --}}
<link rel="stylesheet" type="text/css" href="{{ asset("$pF/css/custom.css") }}">
