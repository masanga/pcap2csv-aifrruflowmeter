	@if (Session::has('errorMessage')) 
		<script type="text/javascript">
			Swal.fire({
			  title: 'Error',
			  text: '{{ Session::get('errorMessage') }}',
			  type: 'error',
			  confirmButtonText: 'OK'
			});
		</script>
		
	@endif

	@if (Session::has('success-message'))
		<script type="text/javascript">
			Swal.fire(
			  'Success',
			  '{{ Session::get('successMessage') }}',
			  'success'
			)
		</script>
	@endif




