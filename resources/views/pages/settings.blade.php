@extends('layouts.app')

@section("pageTitle") {{ "Settings" }} @endsection

@section('content')

	<div class="container-fluid">
		
		<div class="col-md-10 col-md-offset-2">
			<h2>Settings</h2>
			<hr/>

			<form method="POST" action="{{ route('updateSettings') }}">
			
			<table>
				<tr>
					<td>Webhook URL</td>
					<td><input type="text" name="webHookUrl" class="form-control" value="{{ $webHookUrl }}" placeholder="Enter Placeholder URL"></td>
				</tr>

				<tr>
					<td><input type="hidden" name="_token" value="{{ Session::token() }}"></td>
					<td><button class="btn btn-primary form-control">Update Settings</button></td>
				</tr>
			</table>

			</form>
		</div>

	</div>

@endsection