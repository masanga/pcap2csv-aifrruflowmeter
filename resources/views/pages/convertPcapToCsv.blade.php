@extends('layouts.app')

@section("pageTitle") {{ "Convert PCAP to CSV" }} @endsection

@section('content')

<br/>

<style type="text/css">
	table {
		width: 100%;
	}

	table tr td {
		padding-top: 15px;
	}

	.color-red {
		color: red;
	}
</style>

<div class="container">
	
	@if (!Session::has('file_id'))
		<div class="col-md-8 col-md-offset-2">
			<h3>Convert PCAP to CSV </h3>
			@if(!Auth::check()) <span class="color-red">Authentication Required</span> @endif
			<hr/>

			<form action="{{ route('postConvertPcapToCSV') }}" method="POST" enctype="multipart/form-data">
				<table>
					<tr>
						<td>Pcap Name <span class="color-red">*</span></td>
						<td>
							<div class="{{ $errors->has('pcapName') ? 'has-error' : ''}}">
								<input type="text" name="pcapName" class="form-control" placeholder="Pcap Name" value="{{ Request::old('pcapName') }}">
								<span class="help-block">{{ $errors->has('pcapName') ? $errors->first('pcapName') : ''}}</span>
							</div>
						</td>
					</tr>

					<tr>
						<td>Pcap File <span class="color-red">*</span></td>
						<td>
							<div class="{{ $errors->has('pcapFile') ? 'has-error' : ''}}">
								<input type="file" name="pcapFile" value="{{ Request::old('pcapFile') }}" class="form-control">
								<span class="help-block">{{ $errors->has('pcapFile') ? $errors->first('pcapFile') : ''}}</span>
							</div>
						</td>
					</tr>

					<tr>
						<td><input type="hidden" name="_token" value="{{ Session::token() }}"></td>
						<td><input type="submit" value="Convert File" class="btn btn-primary form-control"></td>
					</tr>
				</table>
			</form>
		</div>
	@else
		<div class="col-md-8 col-md-offset-2 center">
			<h2>Download CSV File</h2>
			<hr/>

			<img src="https://image.flaticon.com/icons/png/512/1126/1126902.png" style="width: 250px; height: 250px;">
			<hr/>

			<a href="/public/storage/pcaps/{{ Session::get('file_id') }}/pcap.pcap_ISCX.csv" target="_blank">
				<button class="btn btn-primary form-control">Download CSV File</button>	
			</a>

			<hr/>

			<a href="{{ route('convert-pcap-to-csv') }}">
				<button class="btn btn-success form-control">Convert Another PCAP</button>	
			</a>
			
		</div>
	@endif
</div>


<br/><br/><br/><br/>
<br/><br/><br/><br/>

@endsection