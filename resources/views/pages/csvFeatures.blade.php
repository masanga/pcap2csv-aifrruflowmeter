@extends('layouts.app')

@section("pageTitle") {{ "CSV Features" }} @endsection

@section('content')


<div class="container">
	

<h2>What is AifrruFlowMeter</h2>
<hr/>

The AifrruFlowMeter is an open source tool that generates Biflows from pcap files, and extracts features from these flows.

<br/><br/>

AifrruFlowMeter is a network traffic flow generator available from here . It can be used to generate bidirectional flows, where the first packet determines the forward (source to destination) and backward (destination to source) directions, hence the statistical time-related features can be calculated separately in the forward and backward directions. Additional functionalities include, selecting features from the list of existing features, adding new features, and controlling the duration of flow timeout.

<br/><br/>

NOTE: TCP flows are usually terminated upon connection teardown (by FIN packet) while UDP flows are terminated by a flow timeout. 

<br/><br/>

The flow timeout value can be assigned arbitrarily by the individual scheme e.g., 600 seconds for both TCP and UDP.

	
<h2>List of extracted features and descriptions:</h2>
<hr/>

<table class="table table-bordered table-striped table-hovered">
	<tr>
		<th>S/N</th>
		<th>Feature Name</th>
		<th>Description</th>
	</tr>

	<tr>
		<td>1</td>
		<td>Flow duration</td>
		<td>Duration of the flow in Microsecond</td>
	</tr>

	<tr>
		<td>2</td>
		<td>total Fwd Packet</td>
		<td>Total packets in the forward direction</td>
	</tr>

	<tr>
		<td>3</td>
		<td>total Bwd packets</td>
		<td>Total packets in the backward direction</td>
	</tr>

	<tr>
		<td>4</td>
		<td>total Length of Fwd Packet</td>
		<td>Total size of packet in forward direction</td>
	</tr>

	<tr>
		<td>5</td>
		<td>total Length of Bwd Packet</td>
		<td>Total size of packet in backward direction</td>
	</tr>

	<tr>
		<td>6</td>
		<td>Fwd Packet Length Min</td>
		<td>Minimum size of packet in forward direction</td>
	</tr>

	<tr>
		<td>7</td>
		<td>Fwd Packet Length Max</td>
		<td>Maximum size of packet in forward direction</td>
	</tr>

	<tr>
		<td>8</td>
		<td>Fwd Packet Length Mean</td>
		<td>Mean size of packet in forward direction</td>
	</tr>

	<tr>
		<td>9</td>
		<td>Fwd Packet Length Std</td>
		<td>Standard deviation size of packet in forward direction</td>
	</tr>

	<tr>
		<td>10</td>
		<td>Bwd Packet Length Min</td>
		<td>Minimum size of packet in backward direction</td>
	</tr>

	<tr>
		<td>11</td>
		<td>Bwd Packet Length Max</td>
		<td>Maximum size of packet in backward direction</td>
	</tr>

	<tr>
		<td>12</td>
		<td>Bwd Packet Length Mean</td>
		<td>Mean size of packet in backward direction</td>
	</tr>

	<tr>
		<td>13</td>
		<td>Bwd Packet Length Std</td>
		<td>Standard deviation size of packet in backward direction</td>
	</tr>

	<tr>
		<td>14</td>
		<td>Flow Bytes/s</td>
		<td>Number of flow bytes per second</td>
	</tr>

	<tr>
		<td>15</td>
		<td>Flow Packets/s</td>
		<td>Number of flow packets per second </td>
	</tr>

	<tr>
		<td>16</td>
		<td>Flow IAT Mean</td>
		<td>Mean time between two packets sent in the flow</td>
	</tr>

	<tr>
		<td>17</td>
		<td>Flow IAT Std</td>
		<td>Standard deviation time between two packets sent in the flow</td>
	</tr>

	<tr>
		<td>18</td>
		<td>Flow IAT Max</td>
		<td>Maximum time between two packets sent in the flow</td>
	</tr>

	<tr>
		<td>19</td>
		<td>Flow IAT Min</td>
		<td>Minimum time between two packets sent in the flow</td>
	</tr>

	<tr>
		<td>20</td>
		<td>Fwd IAT Min	</td>
		<td>Minimum time between two packets sent in the forward direction</td>
	</tr>

	<tr>
		<td>21</td>
		<td>Fwd IAT Max</td>
		<td>Maximum time between two packets sent in the forward direction</td>
	</tr>

	<tr>
		<td>22</td>
		<td>Fwd IAT Mean</td>
		<td>Mean time between two packets sent in the forward direction</td>
	</tr>

	<tr>
		<td>23</td>
		<td>Fwd IAT Std</td>
		<td>Standard deviation time between two packets sent in the forward direction</td>
	</tr>

	<tr>
		<td>24</td>
		<td>Fwd IAT Total</td>
		<td>Total time between two packets sent in the forward direction</td>
	</tr>

	<tr>
		<td>25</td>
		<td>Bwd IAT Min</td>
		<td>Minimum time between two packets sent in the backward direction</td>
	</tr>

	<tr>
		<td>26</td>
		<td>Bwd IAT Max</td>
		<td>Maximum time between two packets sent in the backward direction</td>
	</tr>

	<tr>
		<td>27</td>
		<td>Bwd IAT Mean</td>
		<td>Mean time between two packets sent in the backward direction</td>
	</tr>

	<tr>
		<td>28</td>
		<td>Bwd IAT Std</td>
		<td>Standard deviation time between two packets sent in the backward direction</td>
	</tr>

	<tr>
		<td>29</td>
		<td>Bwd IAT Total</td>
		<td>Total time between two packets sent in the backward direction</td>
	</tr>

	<tr>
		<td>30</td>
		<td>Fwd PSH flags</td>
		<td>Number of times the PSH flag was set in packets travelling in the forward direction (0 for UDP)</td>
	</tr>

	<tr>
		<td>31</td>
		<td>Bwd PSH Flags</td>
		<td>Number of times the PSH flag was set in packets travelling in the backward direction (0 for UDP)</td>
	</tr>

	<tr>
		<td>32</td>
		<td>Fwd URG Flags</td>
		<td>Number of times the URG flag was set in packets travelling in the forward direction (0 for UDP)</td>
	</tr>

	<tr>
		<td>33</td>
		<td>Bwd URG Flags</td>
		<td>Number of times the URG flag was set in packets travelling in the backward direction (0 for UDP)</td>
	</tr>

	<tr>
		<td>34</td>
		<td>Fwd Header Length</td>
		<td>Total bytes used for headers in the forward direction</td>
	</tr>

	<tr>
		<td>35</td>
		<td>Bwd Header Length</td>
		<td>Total bytes used for headers in the backward direction</td>
	</tr>

	<tr>
		<td>36</td>
		<td>FWD Packets/s</td>
		<td>Number of forward packets per second</td>
	</tr>

	<tr>
		<td>37</td>
		<td>Bwd Packets/s</td>
		<td>Number of backward packets per second</td>
	</tr>

	<tr>
		<td>38</td>
		<td>Packet Length Min</td>
		<td>Minimum length of a packet</td>
	</tr>

	<tr>
		<td>39</td>
		<td>Packet Length Max</td>
		<td>Maximum length of a packet</td>
	</tr>

	<tr>
		<td>40</td>
		<td>Packet Length Mean</td>
		<td>Mean length of a packet</td>
	</tr>

	<tr>
		<td>41</td>
		<td>Packet Length Std</td>
		<td>Standard deviation length of a packet</td>
	</tr>

	<tr>
		<td>42</td>
		<td>Packet Length Variance</td>
		<td>Variance length of a packet</td>
	</tr>

	<tr>
		<td>43</td>
		<td>FIN Flag Count</td>
		<td>Number of packets with FIN</td>
	</tr>

	<tr>
		<td>44</td>
		<td>SYN Flag Count</td>
		<td>Number of packets with SYN</td>
	</tr>

	<tr>
		<td>45</td>
		<td>RST Flag Count</td>
		<td>Number of packets with RST</td>
	</tr>

	<tr>
		<td>46</td>
		<td>PSH Flag Count</td>
		<td>Number of packets with PUSH</td>
	</tr>

	<tr>
		<td>47</td>
		<td>ACK Flag Count</td>
		<td>Number of packets with ACK</td>
	</tr>

	<tr>
		<td>48</td>
		<td>URG Flag Count</td>
		<td>Number of packets with URG</td>
	</tr>

	<tr>
		<td>49</td>
		<td>CWR Flag Count</td>
		<td>Number of packets with CWR</td>
	</tr>

	<tr>
		<td>50</td>
		<td>ECE Flag Count</td>
		<td>Number of packets with ECE</td>
	</tr>

	<tr>
		<td>51</td>
		<td>down/Up Ratio</td>
		<td>Download and upload ratio</td>
	</tr>

	<tr>
		<td>52</td>
		<td>Average Packet Size </td>
		<td>Average size of packet</td>
	</tr>

	<tr>
		<td>53</td>
		<td>Fwd Segment Size Avg</td>
		<td>Average size observed in the forward direction</td>
	</tr>

	<tr>
		<td>54</td>
		<td>Bwd Segment Size Avg</td>
		<td>Average size observed in the backward direction</td>
	</tr>

	<tr>
		<td>55</td>
		<td>Fwd Bytes/Bulk Avg</td>
		<td>Average number of bytes bulk rate in the forward direction</td>
	</tr>

	<tr>
		<td>56</td>
		<td>Fwd Packet/Bulk Avg</td>
		<td>Average number of packets bulk rate in the forward direction</td>
	</tr>

	<tr>
		<td>57</td>
		<td>Fwd Bulk Rate Avg</td>
		<td>Average number of bulk rate in the forward direction</td>
	</tr>

	<tr>
		<td>58</td>
		<td>Bwd Bytes/Bulk Avg</td>
		<td>Average number of bytes bulk rate in the backward direction</td>
	</tr>

	<tr>
		<td>59</td>
		<td>Bwd Packet/Bulk Avg</td>
		<td>Average number of packets bulk rate in the backward direction</td>
	</tr>

	<tr>
		<td>60</td>
		<td>Bwd Bulk Rate Avg</td>
		<td>Average number of bulk rate in the backward direction</td>
	</tr>

	<tr>
		<td>61</td>
		<td>Subflow Fwd Packets</td>
		<td>The average number of packets in a sub flow in the forward direction</td>
	</tr>

	<tr>
		<td>62</td>
		<td>Subflow Fwd Bytes</td>
		<td>The average number of bytes in a sub flow in the forward direction</td>
	</tr>

	<tr>
		<td>63</td>
		<td>Subflow Bwd Packets</td>
		<td>The average number of packets in a sub flow in the backward direction</td>
	</tr>

	<tr>
		<td>64</td>
		<td>Subflow Bwd Bytes</td>
		<td>The average number of bytes in a sub flow in the backward direction</td>
	</tr>

	<tr>
		<td>65</td>
		<td>Fwd Init Win bytes</td>
		<td>The total number of bytes sent in initial window in the forward direction</td>
	</tr>

	<tr>
		<td>66</td>
		<td>Bwd Init Win bytes</td>
		<td>The total number of bytes sent in initial window in the backward direction</td>
	</tr>

	<tr>
		<td>67</td>
		<td>Fwd Act Data Pkts</td>
		<td>Count of packets with at least 1 byte of TCP data payload in the forward direction</td>
	</tr>

	<tr>
		<td>68</td>
		<td>Fwd Seg Size Min</td>
		<td>Minimum segment size observed in the forward direction</td>
	</tr>

	<tr>
		<td>69</td>
		<td>Active Min</td>
		<td>Minimum time a flow was active before becoming idle</td>
	</tr>

	<tr>
		<td>70</td>
		<td>Active Mean</td>
		<td>Mean time a flow was active before becoming idle</td>
	</tr>

	<tr>
		<td>71</td>
		<td>Active Max</td>
		<td>Maximum time a flow was active before becoming idle</td>
	</tr>

	<tr>
		<td>72</td>
		<td>Active Std</td>
		<td>Standard deviation time a flow was active before becoming idle</td>
	</tr>

	<tr>
		<td>73</td>
		<td>Idle Min</td>
		<td>Minimum time a flow was idle before becoming active</td>
	</tr>

	<tr>
		<td>74</td>
		<td>Idle Mean</td>
		<td>Mean time a flow was idle before becoming active</td>
	</tr>

	<tr>
		<td>75</td>
		<td>Idle Max</td>
		<td>Maximum time a flow was idle before becoming active</td>
	</tr>

	<tr>
		<td>76</td>
		<td>Idle Std</td>
		<td>Standard deviation time a flow was idle before becoming active</td>
	</tr>

</table>
				

</div>

@endsection